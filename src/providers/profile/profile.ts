import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { Headers, RequestOptions } from '@angular/http';


/*
  Generated class for the ProfileProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ProfileProvider {

  constructor(public http: Http) {
    console.log('Hello ProfileProvider Provider');
  }


  doLogin(data){
  	// let userDetails = JSON.parse(localStorage.getItem('user_details'));
  	
  	// if(data.username == userDetails.username && data.password == userDetails.password)
  	// {
  	// 	return data;
  	// }else{
  	// 	return {"state":false,"message":"Username or password wrong"}
  	// }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

      return this.http.post('http://localhost:3000/api/users/login', data, options).toPromise()
        .then((response) => response.json());

  }

  doRegister(data){
    console.log(data);

      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });

      return this.http.post('http://localhost:3000/api/users', data, options).toPromise()
        .then((response) => response.json())
  }

}
