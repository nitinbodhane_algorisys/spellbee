import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { SpellBee } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ListPage } from '../pages/list/list';

import { Http, Response, Headers } from '@angular/http';
import { HttpModule, JsonpModule } from '@angular/http';
import { TextToSpeech } from '@ionic-native/text-to-speech';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { FirstPage } from '../pages/first/first';
import { WordListPage } from '../pages/word-list/word-list';
import { AddwordPage } from '../pages/addword/addword';
import { WordTestPage } from '../pages/word-test/word-test';
import { RegisterPage } from '../pages/register/register';
import { ProfileProvider } from '../providers/profile/profile';


@NgModule({
  declarations: [
    SpellBee,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ListPage,
    LoginPage,
    FirstPage,
    WordListPage,
    AddwordPage,
    WordTestPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(SpellBee),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    SpellBee,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ListPage,
    LoginPage,
    FirstPage,
    WordListPage,
    AddwordPage,
    WordTestPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ProfileProvider,
    TextToSpeech
  ]
})
export class AppModule {}
