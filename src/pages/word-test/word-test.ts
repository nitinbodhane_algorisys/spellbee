import { Component, ViewChild  } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { Slides } from 'ionic-angular';
// import { TextToSpeech } from '@ionic-native/text-to-speech';


/**
 * Generated class for the WordTestPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-word-test',
  templateUrl: 'word-test.html',
})
export class WordTestPage {
	word:any;
	tempword;
	wordArray:any;
	temp;
	index = 0;
	wordList:any;
  flag=false;

  // @ViewChild(Navbar) navBar: Navbar;
  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WordTestPage');
    this.wordList = JSON.parse(localStorage.getItem('wordList'));
  }

  ionViewCanLeave(){
    console.log("ionViewCanLeave")
    window["responsiveVoice"].cancel();
     // this.navCtrl.pop();
    
  }

    slideChanged()
      {
        //let currentIndex = this.slides.getActiveIndex();
        window["responsiveVoice"].cancel();
        //console.log('Current index is', currentIndex)
      }
      
  textToSpeech()
  {
      let currentIndex = this.slides.getActiveIndex();
      console.log(currentIndex)
      let wordArrayLength = this.wordList.length;
      // console.log(wordArrayLength.length)
        if(currentIndex == 0)
        {
          currentIndex = wordArrayLength - 1;
        }else if(currentIndex == (wordArrayLength + 1)){
          currentIndex = 0;
        }else{
          currentIndex -= 1;
        }

        //take string
        var str = this.wordList[currentIndex];          
        // split and will automaticallly convert into array usiing split()
        
        var word = str;

        str = str.split("");

        str.push(word);

        var self = this;

        var i = 0;
        
        function done() {
          i++;
          if (i < str.length) {
            self.speakWord(str[i], "UK English Female", done);
          }
        }

        self.speakWord(str[i], "UK English Female", done); //, done());
        console.log("test");
  }

  //This function is to speak word
  speakWord(wordToSpeak, voice = "UK English Female", onEnd){
    console.log(wordToSpeak);
    window["responsiveVoice"].speak(wordToSpeak, voice, {onend: onEnd});
  }

  repeatWord(){

    let currentIndex = this.slides.getActiveIndex();
      let wordArrayLength = this.wordList.length;
        
        if(currentIndex == 0)
        {
          currentIndex = wordArrayLength - 1;
        }else if(currentIndex == (wordArrayLength + 1)){
          currentIndex = 0;
        }else{
          currentIndex -= 1;
        }

        //take string
        var str = this.wordList[currentIndex];          
        // split and will automaticallly convert into array usiing split()
        
        var word = str;
        str = str.split("");
        str.push(word);

        var self = this;
        var i = 0;
        
          function done() {
           
            i++;
            if (i < str.length) {
              self.speakWord(str[i], "UK English Female", done);
            }
            else if(i==str.length)
              {
                i=0;
                self.speakWord(str[i], "UK English Female", done);
              }
          }

        if(self.flag==false)
          {
            console.log("if");
              self.flag=true;
              self.speakWord(str[i], "UK English Female", done); //, done());
          }else if(self.flag==true){
            console.log("else if");
            self.flag=false;
           window["responsiveVoice"].cancel();
        }
        console.log("test");
      }

  showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Alert',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }
}
