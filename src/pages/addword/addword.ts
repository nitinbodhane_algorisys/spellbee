import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AddwordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addword',
  templateUrl: 'addword.html',
})
export class AddwordPage {

	wordList:any;
	newWord:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.wordList = JSON.parse(localStorage.getItem('wordList'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddwordPage');
  }

  addNewWord(){
  	this.wordList.push(this.newWord); //add value to array.
  	localStorage.removeItem('wordList');
  	// console.log(localStorage.getItem('wordList'));
  	localStorage.setItem('wordList',JSON.stringify(this.wordList)); // to update the localstorage with new word
  	console.log(localStorage.getItem('wordList'))
  	this.navCtrl.pop();

  }

}
