import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LearningMenuPage } from './learning-menu';

@NgModule({
  declarations: [
    LearningMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(LearningMenuPage),
  ],
})
export class LearningMenuPageModule {}
