import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';


/**
 * Generated class for the FirstPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-first',
  templateUrl: 'first.html',
})
export class FirstPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstPage');
  }

  openLoginPage(){
  		this.navCtrl.push(LoginPage);
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Register',
      subTitle: 'Register page has not been created. Try after some time.',
      buttons: ['OK']
    });
    alert.present();
  }

  openRegister(){
  	this.navCtrl.push(RegisterPage);
  }
}
