import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { ProfileProvider } from '../../providers/profile/profile';
import { App } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
	// rootPage:any = TabsPage;

  constructor(public navCtrl: NavController, public appCtrl: App, public navParams: NavParams, private profile:ProfileProvider, public alertCtrl: AlertController) {
  }

  username;
  password;
  user_details = {};

  ionViewDidLoad() {
    this.username = "";
    this.password = "";
    // this.user_details['username'] = this.username;
    // this.user_details['password'] = this.password;
    localStorage.setItem('user_details',JSON.stringify(this.user_details));
    // console.log('ionViewDidLoad LoginPage');
  }


  // This is based on services
  // loginUser(){
  //   // console.log(JSON.parse(localStorage.getItem('user_details')));
  //   var val = {};
  //     val  = {"username":this.username,"password":this.password};
  //   console.log(val)
  //   this.profile.doLogin(val).then((result)=>{
  //         var data = result.data;
  //         console.log(data)
  //       if(data.state === true){
  //            this.navCtrl.push(TabsPage);
  //            console.log(data.message)
  //       }
  //       else{
  //         val = {};
  //         console.log(data.message)
  //         this.showAlert(data.message);
  //       }
  //   });
  // }

    // THis is for localStorage
    loginUser(){
        // get the locallystored credentials
        let localCredentialsArray = JSON.parse(localStorage.getItem('user_credentials'));
        if(localCredentialsArray){

            // console.log(JSON.parse(localStorage.getItem('user_details')));
            var val = {};
              val  = {"username":this.username,"password":this.password};
            // console.log(val)
            
            let flag = false;
            //compare credentials to locally stored value
            for (let i = 0; i < localCredentialsArray.length; i++) {
                if(localCredentialsArray[i].username == this.username && localCredentialsArray[i].password == this.password) {
                    flag = true;
                    // this.navCtrl.push(TabsPage);
                    this.appCtrl.getRootNav().setRoot(TabsPage);

                }
            }

            if(flag === false){
                this.showAlert("Username or password incorrect.");
            }
        }else{
          this.showAlert('Please register first')
        }
      }

  showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Login failed!!!',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

}
