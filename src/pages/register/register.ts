import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ProfileProvider } from '../../providers/profile/profile';
import { LoginPage } from '../login/login';



/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

	username:any;
	password:any;
	confirm_password:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public profile:ProfileProvider, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }


  // Based on services
  // signup(){
  //   if(this.password === this.confirm_password){
  //     let val = {"username":this.username,"password":this.password, "status" : "Active"};
  //     this.profile.doRegister(val).then((data)=>{
  //       this.navCtrl.push(LoginPage);
  //     });

  //   }
  //   else{
  //     this.showAlert();
  //   }
  // }


  // Using localstorage
  signup(){
  	if(this.password === this.confirm_password){
  		  
      //set the credentials
      var arr = []; // this is to store credentials for multiple users
      arr.push({"username":this.username,"password":this.password});
        console.log(arr);
      var val = JSON.stringify(arr);  

      //store credentials locally
      localStorage.setItem('user_credentials', val);

      // redirect to login page
      this.navCtrl.push(LoginPage);
      

  	}
  	else{
	  	this.showAlert();
  	}
  }

  showAlert(){
  	let alert = this.alertCtrl.create({
	      title: 'Register',
	      subTitle: "Password not matched!!!",
	      buttons: ['OK']
	    });
	    alert.present();
  }
}
