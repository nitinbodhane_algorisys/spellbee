import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { AddwordPage } from '../addword/addword';

/**
 * Generated class for the WordListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-word-list',
  templateUrl: 'word-list.html',
})
export class WordListPage {

  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public modalCtrl:ModalController) {
  }

  wordList:any;


  ionViewWillEnter() {
      // You can execute what you want here and it will be executed right before you enter the view
      this.loadData();
  }

  loadData(){
        this.wordList = JSON.parse(localStorage.getItem('wordList'));
        console.log("on page will enter loadData function");
  }

  ionViewDidLoad() {
        // this.wordList = JSON.parse(localStorage.getItem('wordList'));

    console.log(this.wordList)
  }

  addWord(){
  	this.navCtrl.push(AddwordPage);
  }

  doPrompt(word) {
    var self = this;
    let original_word = word;

    let prompt = this.alertCtrl.create({
      title: 'Edit',
      message: "Edit word",
      inputs: [
        {
          name: 'word',
          placeholder: 'enter word',
          value : word
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            let wordArray = JSON.parse(localStorage.getItem("wordList"));
            
            console.log(wordArray);

            for (var i = 0; i < wordArray.length; i++) {
                  if(wordArray[i] == original_word){
                    wordArray[i] = data.word;
                    localStorage.setItem("wordList", JSON.stringify(wordArray));
                    self.loadData();
                    break;
                  }
            }
            console.log(localStorage.getItem("wordList"));
          }
        }
      ]
    });
    prompt.present();
  }

showConfirm(word) {
    var self = this;
    let original_word = word;
    let confirm = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to delete the selected word?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            let wordArray = JSON.parse(localStorage.getItem("wordList"));
            for (var i = 0; i < wordArray.length; i++)
            {
            if (wordArray[i] === original_word)
            { 
            wordArray.splice(i, 1);
            localStorage.setItem("wordList", JSON.stringify(wordArray));
            self.loadData();
            break;
            }
            }    
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  updateItem(data){
    console.log(data)
    this.doPrompt(data)
  }
  
deleteItem(data){
    //this.currentEvent=event;
    console.log(data)
    this.showConfirm(data)
  }
  
}
