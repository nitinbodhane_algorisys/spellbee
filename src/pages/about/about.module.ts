import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutPage } from './about';
import { WordListPage } from '../word-list/word-list';

@NgModule({
  declarations: [
    AboutPage,
    WordListPage
  ],
  imports: [
    IonicPageModule.forChild(AboutPage),
  ],
})
export class AboutPageModule {}
