import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { WordListPage } from '../word-list/word-list';
import { WordTestPage } from '../word-test/word-test';
import { FirstPage } from '../first/first';
import { LoginPage } from '../login/login';
import { TabsPage } from '../tabs/tabs';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  wordList = [];


  constructor(public navCtrl: NavController, public appCtrl: App) {

  }

   ionViewDidLoad() {
  	this.wordList = ["Ant","Go","Eclective","Enervate","Endemic"];
    localStorage.setItem("wordList",JSON.stringify(this.wordList));
      window["responsiveVoice"].cancel();
    
  }


  openWordList(){
    this.navCtrl.push(WordListPage);
  }

  openWordTest(){
    this.navCtrl.push(WordTestPage);
  }

  // logout(){
  //   console.log("logut");
  //   localStorage.clear();
  //   this.navCtrl.push(LoginPage);
  // }

  logout(){
    // this.navCtrl.pop(TabsPage);
    // this.navCtrl.push(LoginPage);
    this.appCtrl.getRootNav().setRoot(FirstPage);
  }

}
