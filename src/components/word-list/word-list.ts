import { Component } from '@angular/core';

/**
 * Generated class for the WordListComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'word-list',
  templateUrl: 'word-list.html'
})
export class WordListComponent {

  text: string;

  constructor() {
    console.log('Hello WordListComponent Component');
    this.text = 'Hello World';
  }

}
