import { NgModule } from '@angular/core';
import { WordListComponent } from './word-list/word-list';
@NgModule({
	declarations: [WordListComponent],
	imports: [],
	exports: [WordListComponent]
})
export class ComponentsModule {}
